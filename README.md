![agent-console.png](https://bitbucket.org/repo/7EEjko8/images/3401981011-agent-console.png)

This is sample minimal Agent Console SDK web app with all functionality of Live Assist. The application consists of a basic web page(index.php) with all the required java script resources, provision.php for creating sessionId, script.js file is for script of the Live Assist functionality and style.css for customized UI.

To run this Agent Console SDK web app:-

1. You need to replace IP address with your product's version IP address in the following file-
index.php 
provision.php
script.js

1. Run on your web server with this URL
localhost/folder_name

In this project, tried to covered the following functionality-

1.  How to setup local and remote video preview.
1.  How to start the voice and video session.
1.  Request screen sharing
1.  End a call when share the screen from agent side
1.  Clearing the remote Video and call disconnect
1.  Co-browse using Short code
1.  How agent can push Images or PDF documents, which is to be displayed on consumer’s device.
1.  How agent can push a link to the consumer.
1.  Tried to work on Agent SDK Callbacks, which shows the current invoked callback in a web page. 

**Product Version Used:-**
FAS - 2.4.1
FCSDK - 3.2.1
LA - 1.45


Here is the google doc link of Agent Console SDK which say about Agent console function and callbacks:-

https://docs.google.com/document/d/1drBTvBK2FFAb2qPBxPPu8uBkGCNYus7VepjAjXhefjs/edit