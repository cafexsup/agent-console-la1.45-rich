/**
 * Responsible for audio/video calling along with co-browsing
 * -and- co-browsing-only, sessions.
 */
var AgentModule = function(sessionId) {
    'use strict';

    var remote, local, sharedDocId, type,
        // jQuery elements for easier event bindings
        $end, $push, $share, $closeSharedDoc,
        $assistedSession, $mode, $clear, $requestShare,
        $annotationControl, $disconnect, $preview;

    // Functions to execute tasks
    cacheDom();
    setClickHandlers();
    bindAgentSdkCallbacks();
    linkUi();

    // Initialise the library and register the Agent to receive calls
    init(sessionId);

    function cacheDom() {
        remote = $('#remote')[0];
        local = $('#local')[0];

        $share = $('#share')[0];
        $end = $('#end');
        $requestShare = $('#requestShare');
        $push = $('.push');
        $closeSharedDoc = $('#closeSharedDoc');
        $assistedSession = $('.assist-session');
        $mode = $('.mode');
        $annotationControl = $('.annotation-control');
        $clear = $('.clear');
        $disconnect = $('#disconnect');
        $preview = $('.preview');
    }

    // Add click handlers that determine what happens when the Agent clicks the relevant button
    function setClickHandlers() {

        $('.shortCode').keyup(function() {
            var $this = $(this);

            if ($this.val() != '' && ($this.val() % 1 === 0)) {
                $('.shortcode').not(this).attr('disabled', 'disabled');
                $('#cobrowseOnly').show();

                $('.shareResize').css({
                    "width": "100%"
                });
            } else {
                $('.shortcode').removeAttr('disabled');
                $('#cobrowseOnly').hide();
                console.log('Please enter numbers only');
            }
        });


        // $('#shortCode').click(function() {
        //     $('#cobrowseOnly').hide();
        // });

        $end.click(function() {
            // Clear the remote video
            $(remote).find('video').attr('src', '');

            CallManager.endCall();
            AssistAgentSDK.endSupport();


            $('.agentTools').hide();
            $('#requestShare').css('visibility', 'hidden');
            $('#end').css('visibility', 'hidden');

            $('.resizeWidth').css({
                "width": "100%"
            });
        });

        $requestShare.click(function() {
            AssistAgentSDK.requestScreenShare();
            $('.agentTools').show();
           
            $('.resizeWidth').css({
                    "width": "70%"
                });
        });

        $disconnect.click(function() {
            AssistAgentSDK.endSupport();
            $('.showCallbacks').text('Call Disconnected');

            $('.agentTools').hide();

            $('.resizeWidth').css({
                "width": "100%"
            });


        });


        $push.submit(function(e) {
            e.preventDefault();

            var $this = $(this);
            var url = $this.find('.url').val();
            var type = $this.find('[name=type]:checked').val();

            switch (type) {
                case 'content':
                    AssistAgentSDK.pushContent(url, function(completionEvent) {
                        type = completionEvent.type;
                        $('.showCallbacks').text('pushContentCallback. Doc ID: ' + type);
                    });

                    break;
                case 'document':
                    AssistAgentSDK.pushDocument(url, function(completionEvent) {
                        sharedDocId = completionEvent.sharedDocId;
                        $('.showCallbacks').text('pushDocumentCallback. Doc ID: ' + sharedDocId);
                    });

                    break;
                case 'link':
                    AssistAgentSDK.pushLink(url, function() {
                        $('.showCallbacks').text('pushLinkCallback');
                    });

                    break;
            }
        });

        // Document close
        $closeSharedDoc.click(function() {
            AssistAgentSDK.closeDocument(sharedDocId);

            $('.showCallbacks').text('closing remote doc. ID: ' + sharedDocId);
        });

        $assistedSession.submit(function(e) {
            e.preventDefault();

            // Get the shortcode from the field
            var $this = $(this);
            var shortCode = $this.find('.shortCode').val();

            $.get('https://cs-creditsuisse.cafex.com:8443/assistserver/shortcode/agent', {
                appkey: shortCode
            }, function(config) {
                var settings = {
                    correlationId: config.cid,
                    url: 'https://cs-creditsuisse.cafex.com:8443',
                    sessionToken: sessionId
                };

                localStorage.setItem("sessionId", sessionId);
                localStorage.setItem("correlationId", settings.correlationId);

                AssistAgentSDK.startSupport(settings);
                AssistAgentSDK.requestScreenShare();
            }, 'json');

            $('#requestShare').css('visibility', 'hidden');
            $('#end').css('visibility', 'hidden');

            $disconnect.show();
            $preview.hide();
        });


        $mode.change(function() {
            var $this = $(this);
            var mode = $this.find('[name=mode]:checked').val();

            // Determine which mode to switch on
            switch (mode) {
                case 'control':
                    AssistAgentSDK.controlSelected();
                    break;

                case 'draw':
                    AssistAgentSDK.drawSelected();
                    break;

                case 'spotlight':
                    AssistAgentSDK.spotlightSelected();
                    break;
            }
        });

        $annotationControl.submit(function(e) {
            e.preventDefault();

            // Extract the vars
            var $this = $(this);
            var color = $this.find('.color').val();
            var opacity = $this.find('.opacity').val();
            var width = $this.find('.width').val();
           // alert(width);

            AssistAgentSDK.setAgentDrawStyle(color, opacity, width);
        });

        $clear.click(function() {
            AssistAgentSDK.clearSelected();
        });

        AssistAgentSDK.setScreenShareActiveCallback(function(active) {
            if (active) {
                $('.showCallbacks').text('setScreenShareActiveCallback');
                $('#end').show();
                $('.agentTools').show();
                $('.resizeWidth').css({
                    "width": "70%"
                });

                $('#disconnect').show();

            } else {
                $('.showCallbacks').text('setScreenShareRejectedCallback');
               // alert();
            }
        });

        AssistAgentSDK.setScreenShareRejectedCallback(function() {
            $('.showCallbacks').text('setScreenShareRejectedCallback');
            $requestShare.disabled = false;
        });

    }

    // Agent SDK's callbacks
    function bindAgentSdkCallbacks() {

        
        AssistAgentSDK.setRemoteViewCallBack(function (x, y) {

            console.log('...Set Remote view callback');
            console.log('x: ' + 1200 + ', y: ' + 720 );
            // var $share = $('#share');

            // var containerHeight = $share.height();
            // var containerWidth = $share.width();
            // var containerAspect = containerHeight/containerWidth;

           // alert(containerAspect);

            // var remoteAspect = y/x;

            // var height;
            // var width;
            // if(containerAspect < remoteAspect) {
            //     height = Math.min(y, containerHeight);
            //     width = height * (x/y);
            // }else{
            //     width = Math.min(x, containerWidth);
            //     height = width * (y/x);
            // }

            //$share.height(height).width(width);


        });


//         AssistAgentSDK.setRemoteViewCallback(function(width, height) {
// var remoteaspect = height / width;
// var localaspect = remoteView.offsetHeight / remoteView.offsetWidth;
// var height;
// var width;
// if (localaspect < remoteaspect){
// height = Math.min(y, remoteView.offsetHeight);
// width = height / remoteaspect;
// } else {
// width = Math.min(x, remoteView.offsetWidth);
// height = width * remoteaspect;
// }
// remoteView.style.height = height + 'px';
// remoteView.style.width = width + 'px';
// });

        AssistAgentSDK.setConnectionEstablishedCallback(function() {
            $('.showCallbacks').text('setConnectionEstablishedCallback');
            $('#requestShare').show();
        });

        AssistAgentSDK.setConnectionLostCallback(function() {
            $('.showCallbacks').text('setConnectionLostCallback');
        });

        AssistAgentSDK.setConnectionReestablishedCallback(function() {
            $('.showCallbacks').text('setConnectionReestablishedCallback');
        });

        AssistAgentSDK.setConnectionRetryCallback(function(retryCount, retryTimeInMilliSeconds) {
            $('.showCallbacks').text('retryCount: ' + retryCount + ' , retryTimeInMilliSeconds: ' + retryTimeInMilliSeconds);
        });

        // When consumer ends the session this callback will be invoked
        AssistAgentSDK.setConsumerEndedSupportCallback(function() {
            $('.showCallbacks').text('setConsumerEndedSupportCallback');
        });

      

        AssistAgentSDK.setConsumerLeftCallback(function () {

            $('.showCallbacks').text('setConsumerLeftCallback');
            var canvasContainer = document.getElementById("share");
             var canvas = (canvasContainer.getElementsByTagName("canvas").length > 0) ? canvasContainer.getElementsByTagName("canvas")[0] : undefined;
            if (canvas) {
             var context = canvas.getContext("2d");
                 context.clearRect(0, 0, canvas.width, canvas.height);
               }

            localStorage.setItem("correlationId", null);
        });

       // context.clearRect(0, 0, canvas.width, canvas.height);

        AssistAgentSDK.setConsumerJoinedCallback(function() {
            $('.showCallbacks').text('setConsumerJoinedCallback');

        });

        AssistAgentSDK.setCallEndedCallback(function() {
            $('.showCallbacks').text('setCallEndedCallback');
        });




    }

    function linkUi() {
        CallManager.setRemoteVideoElement(remote);
        CallManager.setLocalVideoElement(local);
        AssistAgentSDK.setRemoteView(share);
    }

    // Initialise
    function init(sessionId) {
        var config = {
            sessionToken: sessionId,
            autoanswer: false,
            username: 'agent1',
            password: 'none',
            agentName: 'Agent1',
            url: 'https://cs-creditsuisse.cafex.com:8443'
        };

        CallManager.init(config);
    }

}(sessionId);