<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Metadata -->
    <meta charset="UTF-8">
    <title>Agent</title>
    <link rel="icon" type="image/png" href="img/favicon-16x16.png" sizes="16x16">

    <!-- Internal dependencies -->
    <link rel="stylesheet" href="css/style.css" type="text/css">
</head>

<body>
    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="#">CafeX</a>
            </div>

            <ul class="nav navbar-nav pull-right">
                <li class="active" id="shortCode"><a href="#">Co-browse Only</a></li>
                <li id="showBox">
                    <form class="assist-session">
                        <input type="text" class="inp shortCode" placeholder="Enter a Short Code">
                        <button class="btn btn-primary" id="cobrowseOnly">Go</button>
                    </form>
                </li>
            </ul>
        </div>
    </nav>
    <!-- This div will show active callbacks -->
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left ">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left activeCallback">
            Active Status: <span class="showCallbacks"></span>
        </div>
    </div>


    <!-- -->
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left">
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 pull-left leftPanel">
                <div class="agentTools">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left">
                        <div class="row">
                            <h4>Document Push</h4>
                            <form class="push">
                                <input type="text" class="url form-control" placeholder="URL">
                                <input type="radio" name="type" value="document" checked>Document
                                <input type="radio" name="type" value="content">Content
                                <input type="radio" name="type" value="link">Link
                                <button type="submit" class="btn btn-primary">Push</button>
                            </form>
                            <button id="closeSharedDoc" class="btn btn-primary">Close doc</button>
                        </div>
                    </div>
                    <hr>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left">
                        <div class="row">
                            <h4>Mode</h4>
                            <form class="mode">
                                <input type="radio" name="mode" value="control" checked>Control
                                <input type="radio" name="mode" value="draw">Draw
                                <input type="radio" name="mode" value="spotlight">Spotlight
                            </form>
                            <h4>Annotation Control</h4>
                            <form class="annotation-control">
                                <input type="color" class="color form-control" value="#ff0000">
                                <input type="text" class="opacity form-control mtb-5" value="0.5">
                                <input type="text" class="width form-control " value="2">
                                <button type="submit" class="btn btn-primary">Update</button>
                                <button class="clear btn btn-primary">Clear</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left resizeWidth">
                <!-- This div will show screen share -->
                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 pull-left shareResize">
                    <div class="row">
                        <div id="share">
                            <h4>Screen Share</h4>
                        </div>
                        <!-- disconnect for co-browseonly -->
                        <button class="btn btn-info" id="disconnect">Disconnect</button>
                    </div>
                </div>
                <!-- This div is for local and remote preview -->
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 pull-right preview">
                    <div id="local" class="draggable">
                        <h4>Local Preview</h4>
                    </div>
                    <!-- Remote Preview -->
                    <div id="remote" class="draggable">
                        <h4>Remote Preview</h4>
                    </div>
                    
                </div>


                <!-- Request screen share -->
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left">
                        <button id="requestShare" class="btn btn-primary">Request Screen Share</button>
                    </div>
            </div>


            



        </div>
    </div>
</body>
<!-- Required libraries and stylesheets for Assist SDK -->
<link rel="stylesheet" href="https://cs-creditsuisse.cafex.com:8443/assistserver/sdk/web/agent/css/assist-console.css">
<link rel="stylesheet" href="https://cs-creditsuisse.cafex.com:8443/assistserver/sdk/web/shared/css/shared-window.css">
<script src="https://cs-creditsuisse.cafex.com:8443/assistserver/sdk/web/shared/js/thirdparty/i18next-1.7.4.min.js"></script>
<script src="https://cs-creditsuisse.cafex.com:8443/gateway/adapter.js"></script>
<script src="https://cs-creditsuisse.cafex.com:8443/gateway/csdk-sdk.js"></script>

<!-- js required only for co-browse only -->
<script src="https://cs-creditsuisse.cafex.com:8443/assistserver/sdk/web/shared/js/assist-aed.js"></script>
<script src="https://cs-creditsuisse.cafex.com:8443/assistserver/sdk/web/shared/js/shared-windows.js"></script>
<script src="https://cs-creditsuisse.cafex.com:8443/assistserver/sdk/web/agent/js/assist-console.js"></script>
<!-- End! -->

<!-- <script src="js/assist-console-callmanager.js"></script> -->
<script src="https://cs-creditsuisse.cafex.com:8443/assistserver/sdk/web/agent/js/assist-console-callmanager.js"></script>

<script src="https://cs-creditsuisse.cafex.com:8443/assistserver/sdk/web/consumer/assist.js"></script>
<!-- Load jQuery and Bootstrap  -->
<script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<!-- <script src="tabs.js"></script>   -->
<script>
    <?php
            require_once('Provision.php');

            $Provision = new Provision;
            $token = $Provision->provisionAgent('agent1', 'cs-creditsuisse.cafex.com', '.*');

            // Decode the session id from the provisioning token
            $sessionId = json_decode($token)->sessionid;
            ?>

    var sessionId = '<?= $sessionId; ?>';
    console.log('Session ID: ' + sessionId);
</script>
<!-- main js file -->
<script src="js/script.js"></script>
<!-- JS librabry for drag functionality -->
<script src="https://code.interactjs.io/v1.2.6/interact.js"></script>
<script src="js/drag.js" type="text/javascript">
</script>
<script>
    function isNumber(value) {
        // Checks if the value is a positive integer (whole number)
        // Number.isInteger(value) isn't supported by IE
        return typeof value === 'number' && (value % 1) === 0;
    }

    $('#shortCode').click(function(e) {
        $('#showBox').css({
            'width': '200px',
            'height': '50px'
        });
        $('#showBox').animate({
            'width': 'toggle'
        });
    });

    if (localStorage.getItem("sessionId") != null && localStorage.getItem("correlationId") != null) {

        AssistAgentSDK.startSupport({correlationId: localStorage.getItem("correlationId"), sessionToken:  localStorage.getItem("sessionId"), url: 'https://cs-creditsuisse.cafex.com:8443'});
    };

</script>



</html>